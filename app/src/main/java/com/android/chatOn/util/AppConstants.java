package com.android.chatOn.util;

import java.util.regex.Pattern;

/**
 * Created by Supriya A on 7/8/2017.
 */

public class AppConstants {
    public static final String USERNAME_LABEL = "Username";
    public static final String EMAIL_LABEL = "Email";
    public static final String SIGNOUT_LABEL = "Sign out";
    public static final String RESETPASS_LABEL = "Change Password";
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    public static final String NAME = "name" ;
    public static final String EMAIL = "email";
    public static final String AVATA = "avata" ;
    public static final String INTENT_KEY_FRIEND_ID = "friendId";
    public static final String KEY_USER = "user";
    public static final String KEY_EMAIL_ID = "email";


    public static String STR_FRIEND_FRAGMENT = "FRIEND";
    public static String STR_INFO_FRAGMENT = "INFO";

    public static String INTENT_KEY_CHAT_FRIEND = "friendname";
    public static String INTENT_KEY_CHAT_AVATA = "friendavata";
    public static String INTENT_KEY_CHAT_ID = "friendid";
    public static String INTENT_KEY_CHAT_ROOM_ID = "roomid";

    public static final int VIEW_TYPE_USER_MESSAGE = 0;
    public static final int VIEW_TYPE_FRIEND_MESSAGE = 1;

    public static String UID = "";
    public static String STR_DEFAULT_BASE64 = "default";

    public static int ACTION_START_CHAT = 1;

    public static final String KEY_SENDERID = "idSender";
    public static final String KEY_RECEIVER_ID ="idReceiver";
    public static final String KEY_TXT ="text";
    public static final String KEY_TIME_STAMP ="timestamp";
    public static int REQUEST_CODE_REGISTER = 2000;
    public static String STR_EXTRA_ACTION_LOGIN = "login";
    public static String STR_EXTRA_ACTION_RESET = "resetpass";
    public static String STR_EXTRA_ACTION = "action";
    public static String STR_EXTRA_USERNAME = "username";
    public static String STR_EXTRA_PASSWORD = "password";

    public static final String TYPE_LATO_REGULAR = "Lato-Regular.ttf";
    public static final String TYPE_LATO_SEMI_BOLD = "Lato-Semibold.ttf";
    public static final String TYPE_LATO_LIGHT = "Lato-Light.ttf";
}
