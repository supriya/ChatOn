package com.android.chatOn.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.chatOn.R;

public class DialogUtility {

    private static final String LOG_TAG = "DialogUtility";

    public static final class Builder {
        private String title;
        private String message;
        private int iconResId;
        private String iconUrl;
        private String positiveBtnTxt;
        private String negativeBtnTxt;
        private View.OnClickListener positiveBtnClickListener;
        private View.OnClickListener negativeBtnClickListener;

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder message(String message) {
            this.message = message;
            return this;
        }

        public Builder iconResId(int iconResId) {
            this.iconResId = iconResId;
            return this;
        }

        public Builder iconUrl(String iconUrl) {
            this.iconUrl = iconUrl;
            return this;
        }

        public Builder positiveBtnTxt(String positiveBtnTxt) {
            this.positiveBtnTxt = positiveBtnTxt;
            return this;
        }

        public Builder negativeBtnTxt(String negativeBtnTxt) {
            this.negativeBtnTxt = negativeBtnTxt;
            return this;
        }

        public Builder positiveBtnClickListener(View.OnClickListener positiveBtnClickListener) {
            this.positiveBtnClickListener = positiveBtnClickListener;
            return this;
        }

        public Builder negativeBtnClickListener(View.OnClickListener negativeBtnClickListener) {
            this.negativeBtnClickListener = negativeBtnClickListener;
            return this;
        }


    }

    public static ProgressDialog showProgressDialog(Context context, String message) {
        if (context == null) {
            return null;
        }

        ProgressDialog progressDialog = new ProgressDialog(context);

        try {
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setMessage(message);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
            return progressDialog;
        } catch (IllegalArgumentException e) {
            Log.e(LOG_TAG, "Error showing progress dialog");
            ;
        } catch (Exception e) {
            Log.e(LOG_TAG, "Error showing progress dialog");
        }
        return null;
    }

    /**
     * Displays an alert with single button
     *
     * @param context             context
     * @param title               title to be shown in alert
     * @param message             message to be shown in alert
     * @param positiveButtonText  text to be displayed in button
     * @param positiveBtnListener callback for positive button click
     */
    public static void showAlert(Context context, String title, String message, String positiveButtonText, DialogInterface.OnClickListener positiveBtnListener) {
        if (context != null) {

            //set default text for positive button
            if (TextUtils.isEmpty(positiveButtonText)) {
                positiveButtonText = context.getString(android.R.string.ok);
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            if (!TextUtils.isEmpty(title)) {
                builder.setTitle(title);
            }
            builder.setMessage(message);
            builder.setCancelable(false);
            if (TextUtils.isEmpty(positiveButtonText)) {
                positiveButtonText = context.getString(R.string.ok);
            }
            builder.setPositiveButton(positiveButtonText, positiveBtnListener);
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    public static void showToastMessage(Activity activity, String message, int duration) {
        //Creating the LayoutInflater instance
        LayoutInflater li = activity.getLayoutInflater();
        //Getting the View object as defined in the customtoast.xml file
        View layout = li.inflate(R.layout.lyt_toast, null);
        ((TextView)layout.findViewById(R.id.txt_toast_message)).setText(message);

        //Creating the Toast object
        Toast toast = new Toast(activity.getApplicationContext());
        toast.setDuration(duration);
        toast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.BOTTOM, 0, 0);
        toast.setView(layout);
        toast.show();
    }



}
