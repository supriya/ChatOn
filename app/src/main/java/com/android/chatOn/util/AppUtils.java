package com.android.chatOn.util;

import java.util.regex.Matcher;

/**
 * Created by Supriya A on 7/9/2017.
 */

public class AppUtils {

    public static boolean validateEmail(String emailStr) {
        Matcher matcher = AppConstants.VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return  matcher.find();
    }
}
