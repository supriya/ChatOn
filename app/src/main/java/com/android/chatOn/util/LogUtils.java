package com.android.chatOn.util;

import android.util.Log;


/**
 * Created by Supriya A on 7/8/2017.
 */

public class LogUtils {

    private static boolean IS_DEBUG = true;

    /**
     * Prints info log message
     *
     * @param tag - Used to identify the source of a log message
     * @param msg - The message you would like logged.
     */
    public static void i(String tag, String msg) {
        if (IS_DEBUG) {
            Log.i(tag, msg);
        }
    }

    /**
     * Prints debug log message
     *
     * @param tag - Used to identify the source of a log message
     * @param msg - The message you would like logged.
     */
    public static void d(String tag, String msg) {
        if (IS_DEBUG) {
            Log.d(tag, msg);
        }
    }

    /**
     * Prints error log message
     *
     * @param tag - Used to identify the source of a log message
     * @param msg - The message you would like logged.
     */
    public static void e(String tag, String msg) {
        if (IS_DEBUG) {
            Log.e(tag, msg);
        }
    }

    /**
     * Prints the stack trace of exception object passed
     *
     * @param exception Exception object whose stack trace to be printed
     */
    public static void printStackTrace(Exception exception) {
        if (IS_DEBUG) {
            exception.printStackTrace();
        }
    }
}