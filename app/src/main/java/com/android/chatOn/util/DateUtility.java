package com.android.chatOn.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Supriya A on 7/9/2017.
 */

public class DateUtility {



    public static String getDate(long dateInMilliSeconds) {
        if(dateInMilliSeconds == 0)
            return "";
        DateFormat formatter = new SimpleDateFormat("EEE, d MMM yyyy");

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dateInMilliSeconds);
        return formatter.format(calendar.getTime());
    }


    public static String getTime(long dateInMilliSeconds) {
        if(dateInMilliSeconds == 0)
            return "";
        DateFormat formatter = new SimpleDateFormat("hh:mm a");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dateInMilliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static String getDisplayDate(long timestamp) {
        if(timestamp == 0)
            return "";
        DateFormat formatter = new SimpleDateFormat("MMM d");

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        return formatter.format(calendar.getTime());
    }
}
