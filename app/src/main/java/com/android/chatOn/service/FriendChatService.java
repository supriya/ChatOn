package com.android.chatOn.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.android.chatOn.R;
import com.android.chatOn.data.FriendDB;
import com.android.chatOn.model.Friend;
import com.android.chatOn.model.ListFriend;
import com.android.chatOn.util.LogUtils;
import com.android.chatOn.view.activity.MainActivity;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by Supriya A on 7/9/2017.
 */
public class FriendChatService extends Service {
    private static String TAG = "FriendChatService";
    // Binder given to clients
    public final IBinder mBinder = new LocalBinder();
    public Map<String, Boolean> mMapMark;
    public Map<String, Query> mMapQuery;
    public Map<String, ChildEventListener> mMapChildEventListenerMap;
    public ArrayList<String> mListKey;
    public ListFriend mListFriend;

    public FriendChatService() {
    }


    @Override
    public void onCreate() {
        super.onCreate();
        mMapMark = new HashMap<>();
        mMapQuery = new HashMap<>();
        mMapChildEventListenerMap = new HashMap<>();
        mListFriend = FriendDB.getInstance(this).getListFriend();
        mListKey = new ArrayList<>();

        if (mListFriend.getListFriend().size() > 0 ) {
            //subscribe for rooms
            fetchRoomMessage();

        } else {
            stopSelf();
        }
    }

    private void fetchRoomMessage() {
        for (final Friend friend : mListFriend.getListFriend()) {
            if (!mListKey.contains(friend.idRoom)) {
                mMapQuery.put(friend.idRoom, FirebaseDatabase.getInstance().getReference().child("message/" + friend.idRoom).limitToLast(1));
                mMapChildEventListenerMap.put(friend.idRoom, new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                mListKey.add(friend.idRoom);
            }
            mMapQuery.get(friend.idRoom).addChildEventListener(mMapChildEventListenerMap.get(friend.idRoom));
        }
    }

    public void stopNotify(String id) {
        mMapMark.put(id, false);
    }

    public void createNotify(String name, String content, int id) {
        Intent activityIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, activityIntent, PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder notificationBuilder = new
                NotificationCompat.Builder(this)
                .setContentTitle(name)
                .setContentText(content)
                .setContentIntent(pendingIntent)
                .setVibrate(new long[] { 1000, 1000})
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setAutoCancel(true);

        notificationBuilder.setSmallIcon(R.drawable.ic_tab_person);
        NotificationManager notificationManager =
                (NotificationManager) this.getSystemService(
                        Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(id);
        notificationManager.notify(id,
                notificationBuilder.build());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LogUtils.d(TAG, "OnStartService");
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        LogUtils.d(TAG, "OnBindService");
        return mBinder;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        for (String id : mListKey) {
            mMapQuery.get(id).removeEventListener(mMapChildEventListenerMap.get(id));
        }
        mMapQuery.clear();
        mMapChildEventListenerMap.clear();
        LogUtils.d(TAG, "OnDestroyService");
    }

    public class LocalBinder extends Binder {
        public FriendChatService getService() {
            // Return this instance of LocalService so clients can call public methods
            return FriendChatService.this;
        }
    }
}
