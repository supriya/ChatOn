package com.android.chatOn.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.android.chatOn.R;
import com.android.chatOn.data.SharedPreferenceHelper;
import com.android.chatOn.util.LogUtils;
import com.android.chatOn.view.fragment.UserProfileFragment;
import com.android.chatOn.listerners.UserInfoFragmentListener;
import com.android.chatOn.model.Configuration;
import com.android.chatOn.model.User;
import com.android.chatOn.util.AppConstants;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Supriya A on 7/8/2017.
 */

public class UserInfoPresenter {

    private Context mContext;
    private List<Configuration> listConfig = new ArrayList<>();
    private DatabaseReference mUserDB;
    private User mUser;
    private UserInfoFragmentListener mUserInfoFragmentListener;

    public UserInfoPresenter(Context context, UserInfoFragmentListener listener) {
        mContext = context;
        mUserInfoFragmentListener = listener;
        initPresenter();
    }

    public void initPresenter() {
        mUserDB = FirebaseDatabase.getInstance().getReference().child(AppConstants.KEY_USER).child(AppConstants.UID);
        mUserDB.addListenerForSingleValueEvent(userListener);


        SharedPreferenceHelper prefHelper = SharedPreferenceHelper.getInstance(mContext);
        mUser = prefHelper.getUserInfo();

        setupArrayListInfo(mUser);

        mUserInfoFragmentListener.setUserBasicInfo(mUser);

        mUserInfoFragmentListener.setUserInfoRecycler(listConfig);
    }

    private ValueEventListener userListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            listConfig.clear();
            mUser = dataSnapshot.getValue(User.class);
            if(mUser==null) return;
            setupArrayListInfo(mUser);

            mUserInfoFragmentListener.setUserInfoRecycler(listConfig);

            mUserInfoFragmentListener.setUserBasicInfo(mUser);

            SharedPreferenceHelper preferenceHelper = SharedPreferenceHelper.getInstance(mContext);
            preferenceHelper.saveUserInfo(mUser);
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            LogUtils.e(UserProfileFragment.class.getName(), "loadPost:onCancelled");
        }
    };

    /**
     * @param myAccount
     */
    public void setupArrayListInfo(User myAccount) {
        if (myAccount == null) return;
        listConfig.clear();
        if (!TextUtils.isEmpty(myAccount.name)) {
            Configuration userNameConfig = new Configuration(AppConstants.USERNAME_LABEL, myAccount.name, R.mipmap.ic_account_box);
            listConfig.add(userNameConfig);
        }

        if (!TextUtils.isEmpty(myAccount.email)) {
            Configuration emailConfig = new Configuration(AppConstants.EMAIL_LABEL, myAccount.email, R.mipmap.ic_email);
            listConfig.add(emailConfig);
        }
        Configuration signout = new Configuration(AppConstants.SIGNOUT_LABEL, "", R.mipmap.ic_power_settings);
        listConfig.add(signout);
    }

}
