package com.android.chatOn.presenter;


import android.content.Intent;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.android.chatOn.R;
import com.android.chatOn.data.SharedPreferenceHelper;
import com.android.chatOn.model.User;
import com.android.chatOn.util.AppConstants;
import com.android.chatOn.util.LogUtils;
import com.android.chatOn.view.activity.LoginActivity;
import com.android.chatOn.view.activity.MainActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.regex.Matcher;
/**
 * Created by Supriya A on 7/8/2017.
 */
public class LoginPresenter {

    private static String TAG = "LoginActivity";
    LoginActivity mActivity;
    public FirebaseAuth mAuth;
    public FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseUser mUser;
    private boolean mFirstTimeAccess;

    public LoginPresenter(LoginActivity activity) {
        mActivity = activity;
        mFirstTimeAccess = true;
        initFirebase();
    }

    public boolean validate(String emailStr, String password) {
        Matcher matcher = AppConstants.VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return (password.length() > 0 || password.equals(";")) && matcher.find();
    }

    private void initFirebase() {

        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                mUser = firebaseAuth.getCurrentUser();
                if (mUser != null) {
                    // User is signed in
                    AppConstants.UID = mUser.getUid();
                    LogUtils.d(TAG, "onAuthStateChanged:signed_in:" + mUser.getUid());
                    if (mFirstTimeAccess) {
                        mActivity.startActivity(new Intent(mActivity, MainActivity.class));
                        mActivity.finish();
                    }
                } else {
                    LogUtils.d(TAG, "onAuthStateChanged:signed_out");
                }
                mFirstTimeAccess = false;
            }
        };
    }


    public void createUser(String email, String password) {
        mActivity.showProgressDialog(mActivity.getString(R.string.registering));
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(mActivity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        LogUtils.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());
                        mActivity.removeProgressDialog();
                        // If sign in fails, display a message to the mUser. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in mUser can be handled in the listener.
                        if (!task.isSuccessful()) {
                            mActivity.showToastMessage(mActivity.getString(R.string.email_exist_weak_pwd));
                        } else {
                            initNewUserInfo();
                            Toast.makeText(mActivity, "Register and Login success", Toast.LENGTH_SHORT).show();
                            mActivity.startActivity(new Intent(mActivity, MainActivity.class));
                            mActivity.finish();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        mActivity.removeProgressDialog();
                    }
                })
        ;
    }


    /**
     * Action Login
     *
     * @param email
     * @param password
     */
    public void signIn(String email, String password) {
        mActivity.showProgressDialog(mActivity.getString(R.string.signing_in));
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(mActivity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                       LogUtils.d(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());
                        // If sign in fails, display a message to the mUser. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in mUser can be handled in the listener.
                        mActivity.removeProgressDialog();
                        if (!task.isSuccessful()) {
                            mActivity.showToastMessage(mActivity.getString(R.string.wrong_email_pwd_msg));
                        } else {
                            saveUserInfo();
                            mActivity.startActivity(new Intent(mActivity, MainActivity.class));
                            mActivity.finish();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        mActivity.removeProgressDialog();
                    }
                });
    }

    /**
     * Action reset password
     *
     * @param email
     */
    public void resetPassword(final String email) {
        mAuth.sendPasswordResetEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        mActivity.showToastMessage(mActivity.getString(R.string.sent_mail_to) + " " + email);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        mActivity.showToastMessage(mActivity.getString(R.string.false_to_sent_mail_to) + " " + email);
                    }
                });
    }


    public void saveUserInfo() {
        FirebaseDatabase.getInstance().getReference().child("user/" + AppConstants.UID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mActivity.removeProgressDialog();
                User userInfo = dataSnapshot.getValue(User.class);
                SharedPreferenceHelper.getInstance(mActivity).saveUserInfo(userInfo);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    public void initNewUserInfo() {
        User newUser = new User();
        newUser.email = mUser.getEmail();
        newUser.name = mUser.getEmail().substring(0, mUser.getEmail().indexOf("@"));
        newUser.avata = AppConstants.STR_DEFAULT_BASE64;
        FirebaseDatabase.getInstance().getReference().child("user/" + mUser.getUid()).setValue(newUser);
    }

}
