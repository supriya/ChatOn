package com.android.chatOn.presenter;

import android.content.Intent;
import android.support.annotation.NonNull;

import com.android.chatOn.util.AppConstants;
import com.android.chatOn.util.LogUtils;
import com.android.chatOn.view.activity.MainActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * Created by Supriya A on 7/8/2017.
 */
public class MainPresenter {
    private static String TAG = "MainActivity";
    MainActivity mActivity;
    public FirebaseAuth mAuth;
    public FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseUser user;
    private boolean firstTimeAccess;

    public MainPresenter(MainActivity activity) {
        mActivity = activity;
        initFirebase();
    }

    private void initFirebase() {

        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    AppConstants.UID = user.getUid();
                    LogUtils.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                    if (firstTimeAccess) {
                        mActivity.startActivity(new Intent(mActivity, MainActivity.class));
                        mActivity.finish();
                    }
                } else {
                    LogUtils.d(TAG, "onAuthStateChanged:signed_out");
                }
                firstTimeAccess = false;
            }
        };
    }


}
