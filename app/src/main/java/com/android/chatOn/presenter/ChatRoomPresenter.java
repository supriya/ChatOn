package com.android.chatOn.presenter;

import android.content.Intent;

import com.android.chatOn.model.Conversation;
import com.android.chatOn.model.Message;
import com.android.chatOn.util.AppConstants;
import com.android.chatOn.view.activity.ChatRoomActivity;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Supriya A on 7/9/2017.
 */

public class ChatRoomPresenter {
    private final ChatRoomActivity mActivity;
    private String mRoomId;
    private String mIdFriend;
    private Conversation mConversation;

    public ChatRoomPresenter(ChatRoomActivity chatRoomActivity) {
        mActivity = chatRoomActivity;
    }

    public void setIntentData(Intent intent) {

        //fetch intent data
        mIdFriend = intent.getStringExtra(AppConstants.INTENT_KEY_CHAT_ID);
        mRoomId = intent.getStringExtra(AppConstants.INTENT_KEY_CHAT_ROOM_ID);
        String nameFriend = intent.getStringExtra(AppConstants.INTENT_KEY_CHAT_FRIEND);
        mConversation = new Conversation();
        fetchMessages(nameFriend);
    }

    private void fetchMessages(String nameFriend) {
        if (mIdFriend != null && nameFriend != null) {
            mActivity.initialiseViews(nameFriend,mConversation);
            FirebaseDatabase.getInstance().getReference().child("message/" + mRoomId).addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    if (dataSnapshot.getValue() != null) {
                        Message newMessage = dataSnapshot.getValue(Message.class);
                        mConversation.getListMessageData().add(newMessage);
                        mActivity.notifyAdapter();
                        mActivity.updateScroll(mConversation.getListMessageData().size() - 1);
                    }
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            mActivity.setAdapter();
        }
    }

    public void pushMessage(String content) {
        Message newMessage = new Message();
        newMessage.text = content;
        newMessage.idSender = AppConstants.UID;
        newMessage.idReceiver = mRoomId;
        newMessage.timestamp = System.currentTimeMillis();
        FirebaseDatabase.getInstance().getReference().child("message/" + mRoomId).push().setValue(newMessage);
    }

    public void setResult() {
        Intent result = new Intent();
        result.putExtra(AppConstants.INTENT_KEY_FRIEND_ID, mIdFriend);
        mActivity.setResultData(result);
    }
}
