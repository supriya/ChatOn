package com.android.chatOn.presenter;

import android.content.Context;
import android.support.annotation.NonNull;

import com.android.chatOn.R;
import com.android.chatOn.data.FriendDB;
import com.android.chatOn.model.Friend;
import com.android.chatOn.model.ListFriend;
import com.android.chatOn.util.AppConstants;
import com.android.chatOn.util.DialogUtility;
import com.android.chatOn.util.LogUtils;
import com.android.chatOn.view.fragment.FriendListFragment;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by Supriya A on 7/9/2017.
 */

public class FriendListPresenter {

    private final Context mContext;
    private FriendListFragment mFriendListFragment;
    private ListFriend mFriendList = null;
    private ArrayList<String> mListFriendID;

    public FriendListPresenter(Context context, FriendListFragment userFriendListFragment) {
        mContext = context;
        mFriendListFragment = userFriendListFragment;
        initPresenter();
    }

    private void initPresenter() {
        fetchListFromDatabase();
        mFriendListFragment.initialiseAdapter(mFriendList);
        if (mListFriendID == null) {
            mListFriendID = new ArrayList<>();
            LogUtils.d("TAG", "LIST--" + mListFriendID.size());
            mFriendListFragment.showProgressDialog(mContext.getString(R.string.get_all_friends));
            getListFriendUId();
        }
    }

    private void fetchListFromDatabase() {
        if (mFriendList == null) {
            mFriendList = FriendDB.getInstance(mContext).getListFriend();
            if (mFriendList.getListFriend().size() > 0) {
                mListFriendID = new ArrayList<>();
                for (Friend friend : mFriendList.getListFriend()) {
                    mListFriendID.add(friend.id);
                }
            }
        }
    }

    public void onRefresh() {
        mListFriendID.clear();
        mFriendList.getListFriend().clear();
        mFriendListFragment.notifyAdapter();
        FriendDB.getInstance(mContext).dropDB();
        getListFriendUId();
    }


    private void getListFriendUId() {
        FirebaseDatabase.getInstance().getReference().child("friend/" + AppConstants.UID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    HashMap mapRecord = (HashMap) dataSnapshot.getValue();
                    Iterator listKey = mapRecord.keySet().iterator();
                    while (listKey.hasNext()) {
                        String key = listKey.next().toString();
                        mListFriendID.add(mapRecord.get(key).toString());
                    }
                    getAllFriendInfo(0);
                } else {
                    mFriendListFragment.removeProgressDialog();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }


    /**
     * Fetch the friend list data of th user
     * @param index :ID for list
     */
    private void getAllFriendInfo(final int index) {
        if (index == mListFriendID.size()) {
            //save list friend
            mFriendListFragment.notifyAdapter();
            mFriendListFragment.removeProgressDialog();
            mFriendListFragment.setRefreshing(false);
        } else {
            final String id = mListFriendID.get(index);
            FirebaseDatabase.getInstance().getReference().child("user/" + id).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue() != null) {

                        addUser(dataSnapshot, id);
                    }
                    getAllFriendInfo(index + 1);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    private void addUser(DataSnapshot dataSnapshot, String id) {
        Friend user = new Friend();
        HashMap mapUserInfo = (HashMap) dataSnapshot.getValue();
        LogUtils.d("TAG", "DATA CHANGED --" + mapUserInfo.size());
        user.name = (String) mapUserInfo.get(AppConstants.NAME);
        user.email = (String) mapUserInfo.get(AppConstants.EMAIL);
        user.avata = (String) mapUserInfo.get(AppConstants.AVATA);
        user.id = id;
        user.idRoom = id.compareTo(AppConstants.UID) > 0 ? (AppConstants.UID + id).hashCode() + "" : "" + (id + AppConstants.UID).hashCode();
        mFriendList.getListFriend().add(user);
        FriendDB.getInstance(mContext).addFriend(user);
    }


    /**
     * find enter email to start chat
     *
     * @param email :email entered
     */
    public void findIDEmail(String email) {

        mFriendListFragment.showProgressDialog(mContext.getString(R.string.finding_friend));
        FirebaseDatabase.getInstance().getReference().child(AppConstants.KEY_USER).
                orderByChild(AppConstants.KEY_EMAIL_ID).equalTo(email).
                addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mFriendListFragment.removeProgressDialog();
                if (dataSnapshot.getValue() == null) {
                    //email not found
                    DialogUtility.showAlert(mContext, mContext.getString(R.string.failed), mContext.getString(R.string.email_not_found), null, null);
                } else {
                    String id = ((HashMap) dataSnapshot.getValue()).keySet().iterator().next().toString();
                    if (id.equals(AppConstants.UID)) {
                        DialogUtility.showAlert(mContext, mContext.getString(R.string.failed), mContext.getString(R.string.email_not_vaild), null, null);
                    } else {
                        addUserInfo(dataSnapshot, id);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void addUserInfo(DataSnapshot dataSnapshot, String id) {
        HashMap userMap = (HashMap) ((HashMap) dataSnapshot.getValue()).get(id);
        Friend user = new Friend();
        user.name = (String) userMap.get(AppConstants.NAME);
        user.email = (String) userMap.get(AppConstants.EMAIL);
        user.avata = (String) userMap.get(AppConstants.AVATA);
        user.id = id;
        user.idRoom = id.compareTo(AppConstants.UID) > 0 ? (AppConstants.UID + id).hashCode() + "" : "" + (id + AppConstants.UID).hashCode();
        checkBeforeAddFriend(id, user);
    }

    /**
     * check id the ID exists
     */
    private void checkBeforeAddFriend(final String idFriend, Friend userInfo) {

        mFriendListFragment.showProgressDialog(mContext.getString(R.string.add_friend));
        //Check whether id exists in the id list
        if (mListFriendID.contains(idFriend)) {
            mFriendListFragment.removeProgressDialog();
            DialogUtility.showAlert(mContext, mContext.getString(R.string.success), mContext.getString(R.string.user_added_msg, userInfo.email), null, null);
        } else {
            addFriend(idFriend, true);
            mListFriendID.add(idFriend);
            mFriendList.getListFriend().add(userInfo);
            FriendDB.getInstance(mContext).addFriend(userInfo);
            mFriendListFragment.notifyAdapter();
        }
    }

    /**
     * Add friend
     *
     * @param idFriend
     */
    private void addFriend(final String idFriend, boolean isIdFriend) {
        if (idFriend != null) {
            if (isIdFriend) {
                FirebaseDatabase.getInstance().getReference()
                        .child("friend/" + AppConstants.UID)
                        .push()
                        .setValue(idFriend)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    addFriend(idFriend, false);
                                }
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                mFriendListFragment.removeProgressDialog();
                                DialogUtility.showAlert(mContext, mContext.getString(R.string.failed), mContext.getString(R.string.failed_to_add_user), null, null);

                            }
                        });
            } else {
                FirebaseDatabase.getInstance().getReference().child("friend/" + idFriend).
                        push().setValue(AppConstants.UID).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            addFriend(null, false);
                        }
                    }
                })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                mFriendListFragment.removeProgressDialog();
                                DialogUtility.showAlert(mContext, mContext.getString(R.string.failed), mContext.getString(R.string.failed_to_add_user), null, null);
                            }
                        });
            }
        } else {
            mFriendListFragment.removeProgressDialog();
            DialogUtility.showAlert(mContext, mContext.getString(R.string.success), mContext.getString(R.string.succes_to_add_user), null, null);
        }
    }
}
