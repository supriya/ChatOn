package com.android.chatOn.model;

import java.util.ArrayList;


/**
 * Created by Supriya A on 7/8/2017.
 */
public class Conversation {
    private ArrayList<Message> listMessageData;
    public Conversation(){
        listMessageData = new ArrayList<>();
    }

    public ArrayList<Message> getListMessageData() {
        return listMessageData;
    }
}
