package com.android.chatOn.model;


/**
 * Created by Supriya A on 7/8/2017.
 */
public class Message{
    public String idSender;
    public String idReceiver;
    public String text;
    public long timestamp;
}