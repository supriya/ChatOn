package com.android.chatOn.model;


/**
 * Created by Supriya A on 7/8/2017.
 */
public class Status{
    public boolean isOnline;
    public long timestamp;

    public Status(){
        isOnline = false;
        timestamp = 0;
    }
}
