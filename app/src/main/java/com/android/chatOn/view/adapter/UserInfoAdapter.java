package com.android.chatOn.view.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.chatOn.R;
import com.android.chatOn.data.FriendDB;
import com.android.chatOn.listerners.UserInfoFragmentListener;
import com.android.chatOn.model.Configuration;
import com.android.chatOn.service.ServiceUtils;
import com.android.chatOn.util.AppConstants;
import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Supriya A on 7/7/2017.
 */
public class UserInfoAdapter extends RecyclerView.Adapter<UserInfoAdapter.ItemUserInfoViewHolder> {

    private final Activity context;
    private List<Configuration> profileConfig;
    private UserInfoFragmentListener userInfoFragmentListener;

    public UserInfoAdapter(List<Configuration> profileConfig, Activity context, UserInfoFragmentListener infoFragmentListener) {
        this.profileConfig = profileConfig;
        this.context = context;
        userInfoFragmentListener = infoFragmentListener;
    }

    @Override
    public ItemUserInfoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_info_item_layout, parent, false);
        return new ItemUserInfoViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ItemUserInfoViewHolder holder, int position) {
        final Configuration config = profileConfig.get(position);
        holder.label.setText(config.getLabel());
        holder.value.setText(config.getValue());
        holder.icon.setImageResource(config.getIcon());
        holder.label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logoutUser(config);
            }
        });
    }

    private void logoutUser(Configuration config) {
        if (config.getLabel().equals(AppConstants.SIGNOUT_LABEL)) {
            FirebaseAuth.getInstance().signOut();
            FriendDB.getInstance(context).dropDB();
            ServiceUtils.stopServiceFriendChat(context, true);
            if(userInfoFragmentListener!=null){
                userInfoFragmentListener.goToLogin();
            }

        }
    }

    @Override
    public int getItemCount() {
        return profileConfig.size();
    }

    public void updateList(List<Configuration> listConfig) {
        profileConfig = listConfig;
        notifyDataSetChanged();
    }

    class ItemUserInfoViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_title)
        TextView label;

        @BindView(R.id.tv_detail)
        TextView value;

        @BindView(R.id.img_icon)
        public ImageView icon;

        public ItemUserInfoViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
