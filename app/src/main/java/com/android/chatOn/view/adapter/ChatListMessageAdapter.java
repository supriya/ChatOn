package com.android.chatOn.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.chatOn.R;
import com.android.chatOn.data.SharedPreferenceHelper;
import com.android.chatOn.model.User;
import com.android.chatOn.util.AppConstants;
import com.android.chatOn.model.Conversation;
import com.android.chatOn.view.viewholder.ItemMessageFriendHolder;
import com.android.chatOn.view.viewholder.ItemMessageUserHolder;

/**
 * Created by Supriya A on 7/9/2017.
 */

public class ChatListMessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final String mNameFriend;
    private Context mContext;
    private Conversation mConversation;

    public ChatListMessageAdapter(Context context, Conversation consersation, String nameFriend) {
        mContext = context;
        mConversation = consersation;
        mNameFriend = nameFriend;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == AppConstants.VIEW_TYPE_FRIEND_MESSAGE) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_message_friend, parent, false);
            return new ItemMessageFriendHolder(view);
        } else if (viewType == AppConstants.VIEW_TYPE_USER_MESSAGE) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.item_message_user, parent, false);
            return new ItemMessageUserHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemMessageFriendHolder) {
            ((ItemMessageFriendHolder) holder).txtContent.setText(mConversation.getListMessageData().get(position).text);
            if(!TextUtils.isEmpty(mNameFriend))((ItemMessageFriendHolder) holder).displayName.setText(String.valueOf(mNameFriend.toUpperCase().charAt(0)));
        } else if (holder instanceof ItemMessageUserHolder) {
            ((ItemMessageUserHolder) holder).txtContent.setText(mConversation.getListMessageData().get(position).text);
            User user = SharedPreferenceHelper.getInstance(mContext).getUserInfo();
            ((ItemMessageUserHolder) holder).displayName.setText(String.valueOf(getUserName(user)));
        }
    }

    private char getUserName(User user) {
        return user.name != null ? user.name.toUpperCase().charAt(0): user.email.toUpperCase().charAt(0);
    }

    @Override
    public int getItemViewType(int position) {
        return mConversation.getListMessageData().get(position).idSender.equals(AppConstants.UID) ? AppConstants.VIEW_TYPE_USER_MESSAGE : AppConstants.VIEW_TYPE_FRIEND_MESSAGE;
    }

    @Override
    public int getItemCount() {
        return mConversation.getListMessageData().size();
    }
}

