package com.android.chatOn.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.chatOn.R;
import com.android.chatOn.listerners.UserInfoFragmentListener;
import com.android.chatOn.model.Configuration;
import com.android.chatOn.model.User;
import com.android.chatOn.presenter.UserInfoPresenter;
import com.android.chatOn.view.activity.LoginActivity;
import com.android.chatOn.view.adapter.UserInfoAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Supriya A on 7/9/2017.
 */
public class UserProfileFragment extends Fragment implements UserInfoFragmentListener {

    @BindView(R.id.tv_username)
    TextView mUserNameTextView;

    @BindView(R.id.info_recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.displayName)
    TextView mUserName;

    private UserInfoAdapter mInfoAdapter;

    private Context mContext;

    UserInfoPresenter mUserInfoPresenter;

    public UserProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_info, container, false);
        ButterKnife.bind(this, view);
        mContext = view.getContext();
        initView();
        mUserInfoPresenter = new UserInfoPresenter(mContext, this);

        return view;
    }

    private void initView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void setUserBasicInfo(User myAccount) {
        if (myAccount != null) {
            if (!TextUtils.isEmpty(myAccount.name))
                mUserNameTextView.setText(myAccount.name);
            if (!TextUtils.isEmpty(myAccount.name))
                mUserName.setText(String.valueOf(getUserName(myAccount.name)));
        }
    }

    private char getUserName(String user) {
        return user != null ? user.toUpperCase().charAt(0) : user.toUpperCase().charAt(0);
    }

    @Override
    public void setUserInfoRecycler(List<Configuration> listConfig) {
        if (getActivity() == null || isDetached()) return;
        if (listConfig != null && listConfig.size() > 0) {
            if (mInfoAdapter == null) {
                mInfoAdapter = new UserInfoAdapter(listConfig, getActivity(), this);
                mRecyclerView.setAdapter(mInfoAdapter);
            } else {
                if (mInfoAdapter != null) {
                    mInfoAdapter.updateList(listConfig);
                }
            }
        }
    }

    @Override
    public void goToLogin() {
        if (getActivity() == null || isDetached()) return;
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        startActivity(intent);
        getActivity().finish();
    }
}
