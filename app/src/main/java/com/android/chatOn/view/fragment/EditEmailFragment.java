package com.android.chatOn.view.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.android.chatOn.R;
import com.android.chatOn.listerners.EmailEnteredListener;
import com.android.chatOn.util.AppUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Supriya A on 7/9/2017.
 */

public class EditEmailFragment extends DialogFragment implements TextView.OnEditorActionListener, TextWatcher {

    @BindView(R.id.email)
    EditText mEditText;
    @BindView(R.id.ld_error_message)
    TextView mErrorTextMsg;

    private EmailEnteredListener mEmailEnteredListener;

    public static EditEmailFragment newInstance() {
        Bundle args = new Bundle();
        EditEmailFragment fragment = new EditEmailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

     public void setOnEmailEnterListener(EmailEnteredListener listener) {
         mEmailEnteredListener = listener;
     }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().getAttributes().windowAnimations = R.style
                    .DialogAnimation;
            getDialog().getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_enter_username, container);
        ButterKnife.bind(this,view);
        mEditText.requestFocus();
        mEditText.setOnEditorActionListener(this);
        mEditText.addTextChangedListener(this);
        return view;
    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if(mErrorTextMsg.getVisibility() ==  View.VISIBLE){
            mErrorTextMsg.setVisibility(View.GONE);
        }
        return false;
    }


    @OnClick(R.id.btn_ok)
    void OnBtnEnterClick(){
        //check edit text valid edit text
        if(isEmailValid()) {
            mEmailEnteredListener.OnEmailEntered(mEditText.getText().toString().trim());
            getDialog().cancel();
        }
    }

    private boolean isEmailValid() {
        if(TextUtils.isEmpty(mEditText.getText().toString().trim())){
            mErrorTextMsg.setVisibility(View.VISIBLE);
            return false;
        }
        if(!AppUtils.validateEmail(mEditText.getText().toString().trim())){
            //match the pattern
            mErrorTextMsg.setText(getString(R.string.enter_proper_email));
            mErrorTextMsg.setVisibility(View.VISIBLE);
            return false;
        }
        return true;
    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        //check edit text valid edit text
        if(!TextUtils.isEmpty(charSequence) && mErrorTextMsg.getVisibility() ==  View.VISIBLE){
            mErrorTextMsg.setVisibility(View.GONE);
        }
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
