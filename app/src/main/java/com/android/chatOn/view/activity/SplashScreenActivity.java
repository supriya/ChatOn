package com.android.chatOn.view.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.android.chatOn.R;
import com.android.chatOn.util.AppConstants;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * Created by Supriya A on 8/8/2017.
 */
public class SplashScreenActivity extends AppCompatActivity {


    public FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseUser mUser;
    private FirebaseAuth mAuth;
    private boolean mIsLoggedInUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        checkLoginStatus();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    private void checkLoginStatus() {
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                mUser = firebaseAuth.getCurrentUser();
                if (mUser != null) {
                    // User is signed in
                    AppConstants.UID = mUser.getUid();
                    mIsLoggedInUser = true;
                } else {
                    mIsLoggedInUser = false;
                }
                launchNextScreen();

            }
        };
    }

    private void launchNextScreen() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mIsLoggedInUser)
                    redirectToHomeScreen();
                else
                    redirectLoginScreen();
            }
        }, 3000);
    }

    private void redirectToHomeScreen() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void redirectLoginScreen() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
