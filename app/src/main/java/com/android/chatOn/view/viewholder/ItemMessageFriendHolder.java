package com.android.chatOn.view.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.android.chatOn.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Supriya A on 7/7/2017.
 */
public class ItemMessageFriendHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.textContentFriend)
    public TextView txtContent;

    @BindView(R.id.displayName)
    public TextView displayName;

    @BindView(R.id.imageView3)
    public CircleImageView avata;

    public ItemMessageFriendHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
