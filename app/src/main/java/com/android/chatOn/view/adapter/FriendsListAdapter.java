package com.android.chatOn.view.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.chatOn.R;
import com.android.chatOn.listerners.ChatListener;
import com.android.chatOn.model.ListFriend;
import com.android.chatOn.util.DateUtility;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by pc on 7/8/2017.
 */

public class FriendsListAdapter extends RecyclerView.Adapter<FriendsListAdapter.ItemFriendViewHolder> {

    private final ChatListener mChatItemClick;
    private ListFriend mListFriend;
    private Context mContext;

    public static Map<String, Query> mapQuery;
    public static Map<String, DatabaseReference> mapQueryOnline;
    public static Map<String, ChildEventListener> mapChildListener;
    public static Map<String, ChildEventListener> mapChildListenerOnline;
    public static Map<String, Boolean> mapMark;

    public FriendsListAdapter(Context context, ListFriend listFriend, ChatListener onChatListener) {
        mListFriend = listFriend;
        this.mContext = context;
        mapQuery = new HashMap<>();
        mapChildListener = new HashMap<>();
        mapMark = new HashMap<>();
        mapChildListenerOnline = new HashMap<>();
        mapQueryOnline = new HashMap<>();
        mChatItemClick = onChatListener;
    }


    @Override
    public ItemFriendViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_friend, parent, false);
        return new ItemFriendViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ItemFriendViewHolder holder, final int position) {
        final String name = mListFriend.getListFriend().get(position).name;
        final String id = mListFriend.getListFriend().get(position).id;
        final String idRoom = mListFriend.getListFriend().get(position).idRoom;
        holder.txtName.setText(name);
        holder.displayName.setText(String.valueOf(getUserName(name)));

        holder.rootLyt
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mapMark.put(id, null);
                        mChatItemClick.onChatClick(name, idRoom, id);
                    }
                });


        if (mListFriend.getListFriend().get(position).message.text.length() > 0) {

            //message
            holder.txtMessage.setVisibility(View.VISIBLE);
            holder.txtTime.setVisibility(View.VISIBLE);
            if (!mListFriend.getListFriend().get(position).message.text.startsWith(id)) {
                holder.txtMessage.setText(mListFriend.getListFriend().get(position).message.text);
                holder.txtMessage.setTypeface(Typeface.DEFAULT);
                holder.txtName.setTypeface(Typeface.DEFAULT);
            } else {
                holder.txtMessage.setText(mListFriend.getListFriend().get(position).message.text.substring((id + "").length()));
                holder.txtMessage.setTypeface(Typeface.DEFAULT_BOLD);
                holder.txtName.setTypeface(Typeface.DEFAULT_BOLD);
            }

            String time = DateUtility.getDate(mListFriend.getListFriend().get(position).message.timestamp);
            String today = DateUtility.getDate(System.currentTimeMillis());
            if (today.equals(time)) {
                holder.txtTime.setText(DateUtility.getTime(mListFriend.getListFriend().get(position).message.timestamp));
            } else {
                holder.txtTime.setText(DateUtility.getDisplayDate(mListFriend.getListFriend().get(position).message.timestamp));
            }


        } else {
            holder.txtMessage.setVisibility(View.GONE);
            holder.txtTime.setVisibility(View.GONE);
            if (mapQuery.get(id) == null && mapChildListener.get(id) == null) {
                fetchMessageListener(position, id, idRoom);
            } else {
                mapQuery.get(id).removeEventListener(mapChildListener.get(id));
                mapQuery.get(id).addChildEventListener(mapChildListener.get(id));
                mapMark.put(id, true);
            }
        }

    }

    private void fetchMessageListener(final int position, final String id, String idRoom) {
        mapQuery.put(id, FirebaseDatabase.getInstance().getReference().child("message/" + idRoom).limitToLast(1));
        mapChildListener.put(id, new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                HashMap mapMessage = (HashMap) dataSnapshot.getValue();
                if (mapMark.get(id) != null && mListFriend.getListFriend().size() > 0 &&
                        mListFriend.getListFriend().size() >= position &&
                        mListFriend.getListFriend().get(position) != null) {
                    if (!mapMark.get(id) ) {
                        mListFriend.getListFriend().get(position).message.text = id + mapMessage.get("text");
                    } else {
                        mListFriend.getListFriend().get(position).message.text = (String) mapMessage.get("text");
                    }
                    notifyDataSetChanged();
                    mapMark.put(id, false);
                } else {
                    if(mListFriend.getListFriend().size() > 0 && mListFriend.getListFriend().size() >= position && mListFriend.getListFriend().get(position) != null)
                        mListFriend.getListFriend().get(position).message.text = (String) mapMessage.get("text");
                    notifyDataSetChanged();
                }
                if(mListFriend.getListFriend().size() > 0 && mListFriend.getListFriend().size() >= position && mListFriend.getListFriend().get(position) != null)
                    mListFriend.getListFriend().get(position).message.timestamp = (long) mapMessage.get("timestamp");
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        mapQuery.get(id).addChildEventListener(mapChildListener.get(id));
        mapMark.put(id, true);
    }

    private char getUserName(String user) {
        return user != null ? user.toUpperCase().charAt(0): user.toUpperCase().charAt(0);
    }

    @Override
    public int getItemCount() {
        return mListFriend.getListFriend() != null ? mListFriend.getListFriend().size() : 0;
    }

    public class ItemFriendViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.icon_avata)
        CircleImageView avata;
        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.txtTime)
        TextView txtTime;
        @BindView(R.id.txtMessage)
        TextView txtMessage;
        @BindView(R.id.root_lyt)
        LinearLayout rootLyt;
        @BindView(R.id.displayName)
        TextView displayName;
        public ItemFriendViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
