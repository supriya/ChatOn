package com.android.chatOn.view.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.android.chatOn.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Supriya A on 7/9/2017.
 */
public class ItemMessageUserHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.textContentUser)
    public TextView txtContent;

    @BindView(R.id.imageView2)
    public CircleImageView avata;

    @BindView(R.id.displayUserName)
    public TextView displayName;

    public ItemMessageUserHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
