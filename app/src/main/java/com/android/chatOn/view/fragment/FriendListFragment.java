package com.android.chatOn.view.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.chatOn.R;
import com.android.chatOn.presenter.FriendListPresenter;
import com.android.chatOn.util.AppConstants;
import com.android.chatOn.view.activity.ChatRoomActivity;
import com.android.chatOn.view.adapter.FriendsListAdapter;
import com.android.chatOn.listerners.ChatListener;
import com.android.chatOn.listerners.EmailEnteredListener;
import com.android.chatOn.model.ListFriend;
import com.android.chatOn.util.DialogUtility;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by supriya on 7/9/2017.
 */

public class FriendListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, ChatListener, EmailEnteredListener
{

    @BindView(R.id.recycleListFriend)
    RecyclerView mFriendsListView;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;
    protected ProgressDialog mProgressDialog;

    private FriendsListAdapter mFriendsListAdapter;
    private FriendListPresenter mFriendListPresenter;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_friend_list, container, false);
        ButterKnife.bind(this, layout);

        //initialise presenter
        mFriendListPresenter = new FriendListPresenter(layout.getContext(), this);
        initialiseViews();
        return layout;
    }

    private void initialiseViews() {
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mFriendsListView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

    }

    @Override
    public void onRefresh() {
        mFriendListPresenter.onRefresh();
    }

    public void handleOnAddBtnClick() {
        //on click
        EditEmailFragment dialogFragment = new EditEmailFragment().newInstance();
        dialogFragment.setOnEmailEnterListener(this);
        dialogFragment.show(getFragmentManager(),"dialog_fragment");
    }

    @Override
    public void onChatClick(String name, String idRoom, String idFriend) {

        Intent intent = new Intent(getContext(), ChatRoomActivity.class);
        intent.putExtra(AppConstants.INTENT_KEY_CHAT_FRIEND, name);
        intent.putExtra(AppConstants.INTENT_KEY_CHAT_ID, idFriend);
        intent.putExtra(AppConstants.INTENT_KEY_CHAT_ROOM_ID, idRoom);
        this.startActivityForResult(intent, AppConstants.ACTION_START_CHAT);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (AppConstants.ACTION_START_CHAT == requestCode && data != null && FriendsListAdapter.mapMark != null) {
            FriendsListAdapter.mapMark.put(data.getStringExtra(AppConstants.INTENT_KEY_FRIEND_ID), false);
        }
    }

    @Override
    public void OnEmailEntered(String email) {
        mFriendListPresenter.findIDEmail(email);
    }

    public void showProgressDialog(String message) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = DialogUtility.showProgressDialog(getContext(), message);
    }

    public void removeProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    public void initialiseAdapter(ListFriend mFriendList) {
        mFriendsListAdapter = new FriendsListAdapter(getContext(), mFriendList,this);
        mFriendsListView.setAdapter(mFriendsListAdapter);
    }

    public void notifyAdapter() {
        mFriendsListAdapter.notifyDataSetChanged();
    }

    public void setRefreshing(boolean refresh) {
        mSwipeRefreshLayout.setRefreshing(refresh);
    }
}
