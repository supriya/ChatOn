package com.android.chatOn.view.activity;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.chatOn.presenter.LoginPresenter;
import com.android.chatOn.R;
import com.android.chatOn.util.AppConstants;
import com.android.chatOn.util.DialogUtility;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Supriya A on 7/8/2017.
 */
public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.fab)
    public FloatingActionButton mFloatBtnView;

    @BindView(R.id.et_username)
    public EditText mEditTextUsername;

    @BindView(R.id.et_password)
    public EditText mEditTextPassword;

    protected ProgressDialog mProgressDialog;

    LoginPresenter mLoginPresenter;

    @Override
    protected void onStart() {
        super.onStart();
        mLoginPresenter.mAuth.addAuthStateListener(mLoginPresenter.mAuthListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        mLoginPresenter = new LoginPresenter(this);
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (mLoginPresenter.mAuthListener != null) {
            mLoginPresenter.mAuth.removeAuthStateListener(mLoginPresenter.mAuthListener);
        }
    }

    public void showProgressDialog(String message) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }
        mProgressDialog = DialogUtility.showProgressDialog(this, message);
    }

    public void removeProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    public void clickRegisterLayout(View view) {

        getWindow().setExitTransition(null);
        getWindow().setEnterTransition(null);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions options =
                    ActivityOptions.makeSceneTransitionAnimation(this, mFloatBtnView, mFloatBtnView.getTransitionName());
            startActivityForResult(new Intent(this, RegisterActivity.class), AppConstants.REQUEST_CODE_REGISTER, options.toBundle());
        } else {
            startActivityForResult(new Intent(this, RegisterActivity.class), AppConstants.REQUEST_CODE_REGISTER);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppConstants.REQUEST_CODE_REGISTER && resultCode == RESULT_OK) {
            mLoginPresenter.createUser(data.getStringExtra(AppConstants.STR_EXTRA_USERNAME), data.getStringExtra(AppConstants.STR_EXTRA_PASSWORD));
        }
    }

    @OnClick(R.id.bt_go)
    public void clickLogin() {
        String username = mEditTextUsername.getText().toString();
        String password = mEditTextPassword.getText().toString();
        if (mLoginPresenter.validate(username, password)) {
            mLoginPresenter.signIn(username, password);
        } else {
            Toast.makeText(this, R.string.invalid_user_name_password_msg, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(RESULT_CANCELED, null);
        finish();
    }


    public void clickResetPassword(View view) {
        String username = mEditTextUsername.getText().toString();
        if (mLoginPresenter.validate(username, ";")) {
            mLoginPresenter.resetPassword(username);
        } else {
            Toast.makeText(this, R.string.invalid_email, Toast.LENGTH_SHORT).show();
        }
    }

    public void showToastMessage(String message) {
        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_LONG).show();
    }
}
