package com.android.chatOn.view.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.chatOn.R;
import com.android.chatOn.presenter.MainPresenter;
import com.android.chatOn.service.ServiceUtils;
import com.android.chatOn.util.AppConstants;
import com.android.chatOn.view.adapter.ViewPagerAdapter;
import com.android.chatOn.view.fragment.FriendListFragment;
import com.android.chatOn.view.fragment.UserProfileFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Supriya A on 7/8/2017.
 */
public class MainActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    @BindView(R.id.viewpager)
    ViewPager mViewPager;

    @BindView(R.id.tabs)
    TabLayout mTabLayout;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.fab)
    FloatingActionButton mFloatButton;

    private ViewPagerAdapter mAdapter;

    private MainPresenter mMainPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }

        initTab();
        mMainPresenter = new MainPresenter(this);

    }

    @Override
    protected void onStart() {
        super.onStart();
        mMainPresenter.mAuth.addAuthStateListener(mMainPresenter.mAuthListener);
        ServiceUtils.stopServiceFriendChat(getApplicationContext(), false);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mMainPresenter.mAuthListener != null) {
            mMainPresenter.mAuth.removeAuthStateListener(mMainPresenter.mAuthListener);
        }
    }

    @Override
    protected void onDestroy() {
        ServiceUtils.startServiceFriendChat(getApplicationContext());
        super.onDestroy();
    }

    /**
     * init tab view and set up view pager
     */
    private void initTab() {
        mTabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.colorIndivateTab));
        setupViewPager(mViewPager);
        mTabLayout.setupWithViewPager(mViewPager);
        setupTabIcons();
    }


    private void setupTabIcons() {
        int[] tabIcons = {
                R.drawable.ic_tab_person,
                R.drawable.ic_tab_infor
        };
        mTabLayout.getTabAt(0).setIcon(tabIcons[0]);
        mTabLayout.getTabAt(1).setIcon(tabIcons[1]);
    }

    private void setupViewPager(ViewPager viewPager) {
        mAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        mAdapter.addFrag(new FriendListFragment(), AppConstants.STR_FRIEND_FRAGMENT);
        mAdapter.addFrag(new UserProfileFragment(), AppConstants.STR_INFO_FRAGMENT);
        viewPager.setAdapter(mAdapter);
        viewPager.addOnPageChangeListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.about) {
            Toast.makeText(this, R.string.chat_app_version, Toast.LENGTH_LONG).show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.fab)
    void onAddFriendClick() {
        FriendListFragment fragment = (FriendListFragment) mAdapter.getItem(0);
        fragment.handleOnAddBtnClick();
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        ServiceUtils.stopServiceFriendChat(MainActivity.this.getApplicationContext(), false);
        if (mAdapter.getItem(position) instanceof FriendListFragment) {
            mFloatButton.setVisibility(View.VISIBLE);
        } else {
            mFloatButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}