package com.android.chatOn.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.EditText;

import com.android.chatOn.R;
import com.android.chatOn.presenter.ChatRoomPresenter;
import com.android.chatOn.view.adapter.ChatListMessageAdapter;
import com.android.chatOn.model.Conversation;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by Supriya A on 7/9/2017.
 */

public class ChatRoomActivity extends AppCompatActivity {

    @BindView(R.id.recyclerChat)
    RecyclerView mRecyclerChatView;
    @BindView(R.id.editWriteMessage)
    EditText mEditWriteMessage;
    private ChatListMessageAdapter mChatListAdapter;
    private LinearLayoutManager mLinearLayoutManager;
    private ChatRoomPresenter mChatRoomPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
        mChatRoomPresenter = new ChatRoomPresenter(this);
        mChatRoomPresenter.setIntentData(getIntent());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            mChatRoomPresenter.setResult();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        mChatRoomPresenter.setResult();
        this.finish();
    }

    @OnClick(R.id.btnSend)
    void OnSendBtnClick(){
        String content = mEditWriteMessage.getText().toString().trim();
        if (content.length() > 0) {
            mEditWriteMessage.setText("");
            mChatRoomPresenter.pushMessage(content);
        }
    }

    public void initialiseViews(String nameFriend, Conversation conversation) {
        getSupportActionBar().setTitle(nameFriend);
        mLinearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerChatView = (RecyclerView) findViewById(R.id.recyclerChat);
        mRecyclerChatView.setLayoutManager(mLinearLayoutManager);
        mChatListAdapter = new ChatListMessageAdapter(this, conversation,nameFriend);
    }

    public void setResultData(Intent result) {
        setResult(RESULT_OK, result);
        this.finish();
    }

    public void notifyAdapter() {
        mChatListAdapter.notifyDataSetChanged();
    }

    public void updateScroll(int scrollTo) {
        mLinearLayoutManager.scrollToPosition(scrollTo);
    }

    public void setAdapter() {
        mRecyclerChatView.setAdapter(mChatListAdapter);
    }
}


