package com.android.chatOn.listerners;

/**
 * Created by Supriya A on 7/9/2017.
 */
public interface EmailEnteredListener {
    void OnEmailEntered(String email);
}
