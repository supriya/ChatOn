package com.android.chatOn.listerners;

/**
 * Created by Supriya A on 7/9/2017.
 */
public interface ChatListener {
    void onChatClick(String name, String idRoom, String idFriend);
}
