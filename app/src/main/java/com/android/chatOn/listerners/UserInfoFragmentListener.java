package com.android.chatOn.listerners;

import com.android.chatOn.model.Configuration;
import com.android.chatOn.model.User;

import java.util.List;


/**
 * Created by Supriya A on 7/8/2017.
 */
public interface UserInfoFragmentListener {

    void setUserBasicInfo(User myAccount);

    void setUserInfoRecycler(List<Configuration> listConfig);

    void goToLogin();

}
